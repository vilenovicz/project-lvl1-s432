.DEFAULT_GOAL := compile-run

run:
	java -cp ./target/classes first.hello

clean:
	rm -rf ./target

compile: clean
	mkdir -p ./target/classes
	javac -d ./target/classes ./src/main/java/first/hello.java

compile-run: compile run

